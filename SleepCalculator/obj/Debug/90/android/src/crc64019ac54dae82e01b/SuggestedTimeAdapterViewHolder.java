package crc64019ac54dae82e01b;


public class SuggestedTimeAdapterViewHolder
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("SleepCalculator.SuggestedTimeAdapterViewHolder, SleepCalculator", SuggestedTimeAdapterViewHolder.class, __md_methods);
	}


	public SuggestedTimeAdapterViewHolder ()
	{
		super ();
		if (getClass () == SuggestedTimeAdapterViewHolder.class)
			mono.android.TypeManager.Activate ("SleepCalculator.SuggestedTimeAdapterViewHolder, SleepCalculator", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
