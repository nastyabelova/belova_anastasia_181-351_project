﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Views;
using Android.Support.V7.App;
using UK.CO.Chrisjenx.Calligraphy;
using AlertDialog = Android.Support.V7.App.AlertDialog;
namespace SleepCalculator
{
    [Activity(Label = "Night Owl", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        ImageView btnSuggestedTime;
        ImageView btnSpecificTime;
        ImageView milk;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);

            SetContentView (Resource.Layout.Main);

            CalligraphyConfig.InitDefault(new CalligraphyConfig.Builder()
            .SetDefaultFontPath("Fonts/clairehandregular.ttf")
            .SetFontAttrId(Resource.Attribute.fontPath)
            .Build());

            btnSuggestedTime = FindViewById<ImageView>(Resource.Id.ivSleepNow);
            btnSpecificTime = FindViewById<ImageView>(Resource.Id.ivYouPick);
            milk = FindViewById<ImageView>(Resource.Id.glass);

            btnSuggestedTime.Click += BtnSuggestedTime_Click;
            btnSpecificTime.Click += BtnSpecificTime_Click;
            milk.Click += Milk_Click;
         
        }

        private void Milk_Click(object sender, System.EventArgs e)
        {
            var builder = new AlertDialog.Builder(this);

            builder.SetTitle("О приложении")
             .SetMessage("В среднем засыпание занимает около 14 минут, а каждая стадия сна длится около 90 минут. Проснувшись в середине цикла сна, вы чувствуете усталость, а пробуждение между циклами позволяет вам проснуться с ощущением бодрости и бодрости. Это приложение поможет вам установить будильник между циклами сна, чтобы вы чувствовали себя обновленными каждый раз, когда просыпаетесь.")
             .SetNegativeButton("Zzz", delegate { });

            builder.Create().Show();
        }

        private void BtnSuggestedTime_Click(object sender, System.EventArgs e)
        {
            StartActivity(typeof(SuggestedTimeActivity));
        }

        private void BtnSpecificTime_Click(object sender, System.EventArgs e)
        {
            StartActivity(typeof(SpecificTimeActivity));
        }

        protected override void AttachBaseContext(Android.Content.Context @base)
        {
            base.AttachBaseContext(CalligraphyContextWrapper.Wrap(@base));
        }
    }
}

